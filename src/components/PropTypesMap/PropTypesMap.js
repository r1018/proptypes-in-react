import React from "react";
import Person from "./Person";

const aidan = {
  name: "Aidan Walker",
  age: 27,
  email: "aidanw@gmail.com",
  isMarried: false,
  children: [],
};

const john = {
  name: "John Doe",
  age: 29,
  email: "john@gmail.com",
  isMarried: true,
  children: ["Jennie", "Jessica"],
};

const bob = {
  name: "Bob Watson",
  age: 35,
  email: "bob@gmail.com",
  isMarried: true,
  children: ["Ian"],
};
const PropTypesMap = () => {
  return (
    <>
      <Person
        person={aidan}
        // name="Aidan Walker"
        // age={27}
        // email="aidanw@gmail.com"
        // isMarried={false}
        // children={[]}
      />
      <Person person={john} />
      <Person person={bob} />
    </>
  );
};

export default PropTypesMap;
