import React from "react";
import PropTypes from "prop-types";

const Person = ({ person }) => {
  return (
    <div className="person">
      <h3>{person.name}</h3>
      <h3>{person.age}</h3>
      <h3>{person.email}</h3>
      <h3>{person.isMarried}</h3>
      <ul>Children:</ul>
      <ul>
        {person.children.map((child, key) => {
          return <li key={key}>{child}</li>;
        })}
      </ul>
    </div>
  );
};

Person.propTypes = {
  person: PropTypes.shape({
    name: PropTypes.string,
    age: PropTypes.number,
    email: PropTypes.string,
    isMarried: PropTypes.bool,
    children: PropTypes.arrayOf(PropTypes.string),
  }),
};

// const Person = props => {
//   return (
//     <div className="person">
//       <h3>{props.name}</h3>
//       <h3>{props.age}</h3>
//       <h3>{props.email}</h3>
//       <h3>{props.isMarried}</h3>
//       <ul>Children:</ul>
//       <ul>
//         {props.children.map((child, key) => {
//           return <li key={key}>{child}</li>;
//         })}
//       </ul>
//     </div>
//   );
// };

// Person.propTypes = {
//   name: PropTypes.string,
//   age: PropTypes.number,
//   email: PropTypes.string,
//   isMarried: PropTypes.bool,
//   children: PropTypes.arrayOf(PropTypes.string),
// };

export default Person;
