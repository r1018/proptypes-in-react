import React from "react";

const PropTypesDefault = () => {
  return (
    <div className="App">
      <AddItem text="Aidan" number={2} />
      <AddItem text="Joe" />
      <AddItem />
    </div>
  );
};

export default PropTypesDefault;

// 2 ways of writing default props
// function AddItem({ text, number = 4 }) {
//   return (
//     <form>
//       <label for="text-form">Type something:</label>
//       <input type="text" value={text} id="text-form" />
//       <p>{number}</p>
//     </form>
//   );
// }

// 2 ways of writing default props
function AddItem(props) {
  return (
    <form>
      <label for="text-form">Type something:</label>
      <input type="text" value={props.text} id="text-form" />
      <p>{props.number}</p>
    </form>
  );
}

AddItem.defaultProps = {
  number: 2,
  text: "default",
};
