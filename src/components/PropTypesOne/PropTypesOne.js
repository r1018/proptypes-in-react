import React from "react";
import PropTypes from "prop-types";

const PropTypesOne = ({ name, age, renderable }) => {
  //   return `In 5 years ${name} will be ${age + 5}`;
  return renderable;
};

PropTypesOne.propTypes = {
  // name: PropTypes.string.isRequired,
  // age: PropTypes.number.isRequired,
  // types: PropTypes.array, .bool, .func, .object

  // renderable: PropTypes.node, // expects a renderable prop - if we pass in an object - an object is not renderable in react e.g renderable={{a: 1}}
  // renderable: PropTypes.element, // expects a react component only <SomeComponent />

  // children: PropTypes.element.isRequired, // passes a single child to the component
  // as: PropTypes.elementType // <Component as={Link} />

  // name: PropTypes.any.isRequired

  // stringOrNumber: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  // state: PropTypes.oneOf(["Loading", "Ready"]),
  // array: PropTypes.arrayOf(PropTypes.number)
  // array: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string[]))

  // <Component person={{name: "Aidan", age: 27}}
  // defines the shape but it wont error out if we specify more props
  //   person: PropTypes.shape({
  //     name: PropTypes.string,
  //     age: PropTypes.number,
  //   }),
  // props have to be exact, unlike shape
  person: PropTypes.exact({
    name: PropTypes.string,
    age: PropTypes.number,
  }),
};

export default PropTypesOne;
