import logo from "./logo.svg";
import "./App.css";
import PropTypesOne from "./components/PropTypesOne/PropTypesOne";
import PropTypesDefault from "./components/PropTypesDefault/PropTypesDefault";
import PropTypesMap from "./components/PropTypesMap/PropTypesMap";

function App() {
  return (
    <div className="App">
      <PropTypesOne name="Sally" age={20} renderable="Hi" />
      <PropTypesDefault />
      <PropTypesMap />
    </div>
  );
}

export default App;
